import React, { Component } from "react";
import PropTypes from "prop-types";
import "./CardProduct.scss";
import Button from "../button/Button.jsx";
import "../button/Button.scss";
import Icon from "../icon/Icon.jsx";

class CardProduct extends Component {
  cartCheck() {
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let newCartProds = cartProds.map((el) => el);
    let result = newCartProds.includes(this.props.card.id);

    return result;
  }

  render() {
    const { card, onClick, classNameIcon, favorites } = this.props;

    return (
      <div key={card.id} className="product-card" id={card.id}>
        <h3 className="product-name">{card.ProductName}</h3>
        <div className="product-card-container">
          <div>
            <img
              src={card.link}
              style={{ width: 180, height: "auto" }}
              alt={card.ProductName}
            />
          </div>
          <div>
            <p className="product-article">Model: {card.type}</p>
            <p>Price: {card.price} USD</p>
            <p>Color: {card.color}</p>
          </div>
        </div>
        <div id={card.id} className="product-activity">
          <Button
            id={card.id}
            className={` btn ${
              this.cartCheck() ? "btn__added-to-card" : "btn__add-to-card"
            }`}
            dataModal={"modalID2"}
            text={`${this.cartCheck() ? "Added to cart" : "Add to cart"}`}
            onClick={this.cartCheck() ? null : onClick}
          />
          <Icon id={card.id} className={classNameIcon} onClick={favorites} />
        </div>
      </div>
    );
  }
}

CardProduct.propTypes = {
  card: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  classNameIcon: PropTypes.string,
  favorites: PropTypes.func,
};
CardProduct.defaultProps = {
  className: "favor-icon",
};

export default CardProduct;
