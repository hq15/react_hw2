import React from "react";
import PropTypes from "prop-types";
import "./Modal.scss";

class Modal extends React.Component {
  componentDidMount() {
    this.props.show();
  }

  componentWillUnmount() {
    this.props.hidden();
  }

  render() {
    const { className, header, closeButton, onClick, description, actions } =
      this.props;

    return (
      <>
        <div className={className}>
          <div className="modal__header">
            <h3>{header}</h3>
            {closeButton && (
              <button className="btn__close" onClick={onClick}>
                X
              </button>
            )}
          </div>
          <p className="modal__text">{description}</p>
          {actions}
        </div>
      </>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  actions: PropTypes.object.isRequired,
  closeButton: PropTypes.bool,
};

Modal.defaultProps = {
  closeButton: false,
};

export default Modal;
