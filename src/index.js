import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import Forms from "./App.jsx";

ReactDOM.render(
  <React.StrictMode>
    <Forms />
  </React.StrictMode>,
  document.getElementById("root")
);
